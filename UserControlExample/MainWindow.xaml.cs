﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

//git clone https://kwarwas@bitbucket.org/kwarwas/wpfusercontrolexample.git
//https://bitbucket.org/kwarwas/wpfusercontrolexample/downloads/

namespace UserControlExample
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            DataContext = new GeneralViewModel
            {
                Person = new PersonViewModel
                {
                    FirstName = "Jan",
                    LastName = "Kowalski"
                },
                HomeAddress = new Address
                {
                    City = "BB"
                },
                WorkAddress = new Address
                {
                    City = "Katowice"
                }
            };
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var general = DataContext as GeneralViewModel;
            general.Person.FirstName = "Justyna";
        }
    }

    class GeneralViewModel
    {
        public PersonViewModel Person { get; set; }
        public Address HomeAddress { get; set; }
        public Address WorkAddress { get; set; }
    }

    class PersonViewModel : INotifyPropertyChanged
    {
        private string _firstName;
        public string FirstName
        {
            get
            {
                return _firstName;
            }
            set
            {
                if (value != _firstName)
                {
                    _firstName = value;
                    OnPropertyChanged();
                }
            }
        }

        private void OnPropertyChanged([CallerMemberName]string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public string LastName { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
    }

    class Address
    {
        public string City { get; set; }
    }
}
